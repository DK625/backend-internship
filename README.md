# BACKEND INTERNSHIP PROGRAM

## 1. Yêu cầu
### 1.1 Tuần 1: API
Bạn hãy thiết kế cơ sở dữ liệu, thiết kế API và viết code để thực hiện các yêu cầu sau.  

Công nghệ:
- Ngôn ngữ: Python
- API Framework: Flask/FastAPI
- Database: PostgreSQL
- ORM: SQLAlchemy
- Migration Tool: Alembic
- Các thư viện khác: Bạn có thể sử dụng các thư viện khác nếu cần thiết

Mô tả:  
- Một người dùng có thể đăng ký tài khoản với các thông tin(tên, email, mật khẩu).  
- Mật khẩu của người dùng cần được bảo vệ bằng cách mã hóa, để đảm bảo dù có bị lộ thì mật khẩu cũng không thể đoán được.
- Người dùng có thể đăng nhập vào hệ thống bằng email và mật khẩu đã đăng ký.  
- Sau khi đăng nhập thành công, người dùng có thể thêm các todo list vào hệ thống.  
- Một người dùng có nhiều todo list. Mỗi todo list có các thông tin sau:
  - `name`: Tên của todo, không được để trống, không được trùng với các list khác của người dùng
  - `description`: Thông tin chi tiết của list, có thể để trống
- Một todo list có nhiều todo. Mỗi todo có các thông tin sau:
  - `title`: Tiêu đề của todo, không được để trống
  - `description`: Thông tin chi tiết của todo, có thể để trống
  - `due_date`: Ngày hết hạn của todo, có thể để trống
  - `status`: Trạng thái của todo, có 2 giá trị là Chưa hoàn thành và Hoàn thành, mặc định là Chưa hoàn thành  

- Mỗi người dùng chỉ có thể xem được các todo list và todo của mình.
- Người dùng có thể xem danh sách các todo list của mình.
- Người dùng có thể xóa các todo list của mình. Nếu xóa các todo list, các todo trong todo list cũng bị xóa theo.
- Người dùng có thể xem danh sách các todo trong một list của mình theo trạng thái(toàn bộ, hoàn thành, chưa hoàn thành).
- Người dùng có thể thêm todo vào một list của mình.
- Người dùng có thể xóa todo trong một list của mình.
- Người dùng có thể đánh dấu todo đã hoàn thành. Khi người dùng đánh dấu todo là hoàn thành thì ngày hoàn thành của todo sẽ được cập nhật thành thời điểm hiện tại.
- Người dùng có thể đánh dấu todo chưa hoàn thành. Khi người dùng đánh dấu todo là chưa hoàn thành thì ngày hoàn thành của todo sẽ được cập nhật thành rỗng.

Output cần báo cáo:
- Sơ đồ ERD(Entity Relationship Diagram) của cơ sở dữ liệu bạn đã thiết kế, và giải thích lý do tại sao lại thiết kế như vậy
- API Document, có thể dùng Postman, Swagger, OpenAPI, ... để tạo ra API Document, ưu tiên sử dụng Swagger
- Source code của API
- Các câu lệnh để tạo database, tạo user, cấp quyền cho user với database, chạy migration, chạy API
- Toàn bộ Sơ đồ ERD(file PDF, API Document, API Source code) cần được đưa lên project này theo hướng dẫn ở phần 2

### 1.2 Tuần 2: Background Jobs
### 1.3 Tuần 3: Caching
### 1.4 Tuần 4: Deployment
### 1.5 Nâng cao

## 2. Hướng dẫn
Ở tuần đầu tiên, bạn cần tạo branch mới từ branch `master` và đặt tên là `week1`. Sau đó, bạn cần tạo pull request từ branch `week1` đến branch `master`.  
Với các tuần tiếp theo, bạn cần tạo branch mới từ branch của tuần trước. Ví dụ, bạn cần tạo branch `week2` từ branch `week1`. Sau đó, bạn cần tạo pull request từ branch `week2` đến branch `week1`.  
Sau khi hoàn thành bạn gửi lại link tới Merge Request cho mentor
Mentor sẽ review, yêu cầu chỉnh sửa nếu có.  
Nếu cần chỉnh sửa, bạn chỉnh sửa trên branch đang làm việc, không cần tạo branch mới.